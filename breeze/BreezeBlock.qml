import QtQuick 2.1
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.1


import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents


Item {
    property alias main: view.sourceComponent
    property alias controls: controlsLayout.sourceComponent
    readonly property alias mainItem: view.item
    readonly property alias controlsItem: controlsLayout.item

    property bool canShutdown: false
    property bool canReboot: false

    Rectangle {
        color: "#000"
        opacity: 0.3
        anchors {
            fill: parent
        }
    }

    Loader {
        id: view
        anchors {
            margins: units.largeSpacing

            left: parent.left
            right: parent.right
            top: parent.top
            bottom: separator.top
        }
    }

    Rectangle {
        id: separator
        height: 1
        color: "#EEEEEE"
        width: parent.width
        anchors {
            margins: units.largeSpacing

            bottom: controlsLayout.top
        }
    }
    Loader {
        id: controlsLayout
        anchors {
            margins: units.largeSpacing

            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
    }
}
