import QtQuick 2.1
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.1 as Controls

import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents

Image {
    id: root
    width: 1000
    height: 1000

    source: "background.png" //FIXME clearly don't hardcode this
    smooth: true
    property bool debug: false

    Rectangle {
        id: debug3
        color: "green"
        visible: debug
        width: 3
        height: parent.height
        anchors.horizontalCenter: root.horizontalCenter
    }

    Controls.StackView {
        id: stackView

        height: units.largeSpacing*14
        anchors.centerIn: parent

        initialItem: BreezeBlock {
            id: loginBlock
            main: UserSelect {
                id: usersSelection
                model: userModel
            }

            controls: Item {
                function startLogin () {
                    sddm.login(loginBlock.mainItem.selectedUser, passwordInput.text, sessionCombo.currentIndex)
                }

                height: childrenRect.height
                Layout.fillWidth: true

                PlasmaComponents.ComboBox {
                    id: sessionCombo
                    model: sessionModel
                    currentIndex: sessionModel.lastIndex
                    anchors.left: parent.left

                    width: 200

                    textRole: "name"
                }

                RowLayout {
                    anchors.horizontalCenter: parent.horizontalCenter
                    PlasmaComponents.TextField {
                        id: passwordInput
                        placeholderText: i18n("Password")
                        echoMode: TextInput.Password
                        onAccepted: startLogin()
                        focus: true
                    }

                    PlasmaComponents.Button {
                        Layout.minimumWidth: passwordInput.width
                        text: i18n("Login")
                        onClicked: startLogin();
                    }
                }

                PlasmaComponents.ToolButton {
                    id: restartButton
                    anchors.right: parent.right
                    iconSource: "system-reboot"

                    onClicked: {
                        stackView.push(logoutScreenComponent, {"mode": "reboot"})
                    }
                }

                PlasmaComponents.ToolButton {
                    anchors.right: restartButton.left
                    anchors.rightMargin: 5
                    iconSource: "system-shutdown"

                    onClicked: {
                        stackView.push(logoutScreenComponent, {"mode": "shutdown"})
                    }

                }
            }

            Component {
                id: logoutScreenComponent
                LogoutScreen {
                    onCancel: stackView.pop()

                    onShutdownRequested: {
                        sddm.powerOff()
                    }

                    onRebootRequested: {
                        sddm.reboot()
                    }
                }
            }
        }
    }
}
