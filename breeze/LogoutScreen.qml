import QtQuick 2.1
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.1 as Controls

import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents

BreezeBlock {
    id: root
    property string mode: "shutdown"
    property var currentAction
    property real remainingTime: 30

    signal cancel()
    signal shutdownRequested()
    signal rebootRequested()

    Controls.Action {
        onTriggered: root.cancel()
        shortcut: "Escape"
    }

    onRemainingTimeChanged: {
        if(remainingTime<0)
            root.currentAction()
    }
    Timer {
        running: true
        repeat: true
        interval: 1000
        onTriggered: remainingTime--
    }

    main: ColumnLayout {
        PlasmaComponents.Label {
            id: actionLabel
            Layout.alignment: Qt.AlignHCenter
        }

        PlasmaComponents.ToolButton {
            id: actionIcon
            Layout.alignment: Qt.AlignHCenter
            Layout.fillHeight: true
            onClicked: root.currentAction()
        }

        PlasmaComponents.ProgressBar {
            id: progressBar
            Layout.alignment: Qt.AlignHCenter
            minimumValue: 0
            maximumValue: 30
            value: root.remainingTime
        }

        PlasmaComponents.Label {
            anchors.right: progressBar.right
            text: i18n("In %1 seconds", root.remainingTime);
        }

        state: mode
        states: [
            State {
                name: "shutdown"
                PropertyChanges { target: root; currentAction: shutdownRequested }
                PropertyChanges { target: actionLabel; text: ("Shutting down") }
                PropertyChanges { target: actionIcon; iconSource: ("system-shutdown") }
            },
            State {
                name: "reboot"
                PropertyChanges { target: root; currentAction: rebootRequested }
                PropertyChanges { target: actionLabel; text: ("Rebooting") }
                PropertyChanges { target: actionIcon; iconSource: ("system-reboot") }
            }
        ]
    }

    controls: Item {
        Layout.fillWidth: true
        height: buttons.height

        RowLayout {
            id: buttons
            anchors.centerIn: parent

            PlasmaComponents.Button {
                text: i18n("Cancel")
                onClicked: root.cancel()
            }

            PlasmaComponents.Button {
                id: commitButton
                onClicked: root.currentAction()
            }
        }

        PlasmaComponents.ToolButton {
            id: restartButton
            anchors.right: parent.right
            iconSource: "system-reboot"
            enabled: root.mode != "reboot"

            onClicked: {
                root.mode = "reboot"
            }
        }

        PlasmaComponents.ToolButton {
            anchors.right: restartButton.left
            anchors.rightMargin: 5
            iconSource: "system-shutdown"
            enabled: root.mode != "shutdown"

            onClicked: {
                root.mode = "shutdown"
            }

        }

        state: mode
        states: [
            State {
                name: "shutdown"
                PropertyChanges { target: commitButton; text: ("Shut down") }
            },
            State {
                name: "reboot"
                PropertyChanges { target: commitButton; text: ("Reboot") }
            }
        ]
    }
}
