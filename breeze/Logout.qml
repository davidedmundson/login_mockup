import QtQuick 2.1

Image {
    source: "background.png" //FIXME clearly don't hardcode this
    smooth: true

    LogoutScreen {
        anchors {
            verticalCenter: parent.verticalCenter
            left: parent.left
            right: parent.right
        }

        height: units.largeSpacing*14

        onShutdownRequested: {
            callshit.powerOff()
        }

        onRebootRequested: {
            callshit.reboot();
        }

        onCancel: Qt.quit()
    }
}
